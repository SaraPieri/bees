**Bees**

Current evidence shows that we see a reduction in _biological diversity_ at an astonishing rate. However, some natural ecosystem services are vital to human societies. Among these is pollination, carried out both by wild organisms that live free (such as bees but also flies or moths) and by the bee species currently on the market. Bees represent the predominant group of pollinators and play a fundamental role for our planet, guaranteeing food security and maintaining biodiversity. Up to 35% of world food production depends on beekeepers' role, on about 100 plant species that provide 90% of food worldwide; 71 of these are linked to their pollination work. Furthermore, pollinators' economic contribution to human food amounts to € 153 billion globally, representing about 9.5% of the total value of human food production in the world. 

<img src="./supermercato-e-api.jpg" alt="drawing" width="600"/>

In the last decade, we have been witnessing a depopulation of hives (SSA or more commonly CCD, or Colony Collapse Disorder), which seems to concern mostly the colonies of Apis Mellifera, the most widespread in the world, native to Europe, Africa, and part of Asia, and introduced in the American and Australian continents. 

 This phenomenon is not yet well understood, but causes are attributable to many factors, such as the massive use of pesticides for agricultural purposes, genetic modification of plantations, habitat fragmentation and degradation, climate change, and pest infestations.

<br >

**The Goal**

This work aims to pave the way for early identification of the Varroa parasite through Deep Learning and Artificial Neural Networks. This technology allows recognizing the parasite's presence on the bee's body through video or images without the need for direct human intervention. The project aims to fit into the context of intelligent monitoring of the hive to protect and enhance the free and extraordinary work that these small insects do for us every day.

<img src="./ape_varroa.jpg" alt="drawing" width="250"/>
<img src="./Corretto.png" alt="drawing" width="350"/>

<br >
<br >


**Project Structure**

 This project includes: 

- A brief introduction to the state-of-the-art in the field of Deep Learning and Neural Networks.
- A theoretical explanation of the elements used in the discussion.
- Analysis of the chosen dataset and the experiments carried out.
- A new proposal to tackle the problem.
